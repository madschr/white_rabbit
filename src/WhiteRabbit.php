<?php

// todo: Skift parseFile og findMedianLetter til private

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>
            $this->findMedianLetter($this->parseFile($filePath),$occurrences), "count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    public function parseFile ($filePath)
    {
        $parsedFile = file_get_contents($filePath);

        return $parsedFile;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    public function findMedianLetter($parsedFile, &$occurrences)
    {
        $medianArray = [];
        $count = 0;

        // iterate range of a - z
        // count the occourences of the current letter (in lower case) in the given file
        // add a key value pair to array. key = current letter, value = occourences of current letter
        foreach(range("a", "z") as $currentLetter) {
            $count++;
            $currentLetterOccurrences = substr_count(strtolower($parsedFile), $currentLetter);

            $medianArray[$currentLetter] = $currentLetterOccurrences;
        }

        // sort array ascending
        asort($medianArray);

        // get size of array
        $arraySize = count($medianArray);

        // get middle value in array
        $middleVal = floor(($arraySize - 1) / 2);

        // since the alphabet is 26 letters long and therefore there is no "middle value",
        // get index of two middle values
        $lowMidIndex = $middleVal;
        $highMidIndex = $middleVal + 1;

        $keys = array_keys($medianArray);

/*        for($i = 0; $i < sizeof($medianArray); $i++) {
            print("i: $i" . " key: " . $keys[$i] . " value: " . $medianArray[$keys[$i]] . "\n");
        }*/

        $lowMidValue = $medianArray[$keys[$lowMidIndex]];
        $highMidValue = $medianArray[$keys[$highMidIndex]];

        $median = ($lowMidValue + $highMidValue) / 2;

        print($median);

//        print("Key: " . $keys[$lowMidIndex] . ", Value: " . $medianArray[$keys[$lowMidIndex]]);
//        print("\nKey: " . $keys[$highMidIndex] . ", Value: " . $medianArray[$keys[$highMidIndex]]);

//        var_dump($medianArray);

        return $medianArray;
    }
}

$wR = new WhiteRabbit;

$testFile = $wR -> parseFile(__DIR__ . "./../txt/text1.txt");

//print($wR -> parseFile($testFile));
//foreach(range("a", "z") as $elements) {
//    print($elements);
//}


$wR->findMedianLetter($testFile, $occurrences);